---
title: Kali Linux installation requirements
description:
icon:
date: 2020-03-07
type: post
weight:
author: ["gamb1t",]
tags: ["",]
keywords: ["",]
og_description:
---

The installation requirements for Kali Linux vary depending on what you would like to install. On the low end, you can set up Kali as a basic Secure Shell (SSH) server with no desktop, using as little as 128 MB of RAM (512 MB recommended) and 2 GB of disk space. On the higher end, if you opt to install the default XFCE4 desktop and the kali-linux-default meta-package, you should really aim for at least 2048 MB of RAM and 20 GB of disk space.